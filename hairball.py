from dis import dis
import time
import logging
import streamlit as st
from rich.logging import RichHandler
from regurgitator import regurgitate
import requests

APP_NAME = "Hairball"
CRAPI_URI = "https://api.crossref.org"


logging.basicConfig(level=logging.WARNING, handlers=[RichHandler()])
logger = logging.getLogger("rich")

logger.info(f"starting: {APP_NAME}")



def load_file(fn):
    with open(fn) as f:
        return f.read()


def any_info_missing():
    return any(st.session_state[key] == "" for key in ["doi", "name", "email"])


def timestamp():
    return int(time.time())

@st.experimental_memo(show_spinner=False, max_entries=50)
def doi_agency(doi):
    res = requests.get(f"{CRAPI_URI}/works/{doi}/agency")
    return res.json()["message"]["agency"]["id"] if res.status_code == 200 else None

@st.experimental_memo(show_spinner=False,max_entries=50)
def hairball(doi,depositor_name,email_address):
    """ A cached proxy for regurgitate """
    schema_version, xml = regurgitate(
            doi=doi,
            doi_batch_id=timestamp(),
            timestamp=timestamp(),
            depositor_name=depositor_name,
            email_address=email_address,
            registrant=f" {depositor_name} via {APP_NAME}",
        )
    return schema_version, xml

def check_inputs():
    if any_info_missing():
        st.warning(
            "You have not provided all the needed information. Please fillout all the fields in the sidebar"
        )
        return

    agency = doi_agency(st.session_state.doi)
    if agency is None:
        st.warning("You have not provided a valid DOI")
        return
    elif agency not in ["public", "crossref"]:
        st.warning(f"That doi is registered with: {agency}")
        return
    st.info("Ready to regurgitate")
    st.session_state.ready = True

    # attempt_to_regurgitate()
def attempt_to_regurgitate():


    check_inputs()
    if  "ready" not in st.session_state:
        return  

    with st.spinner(text=f"Attempting to recreate deposit file for: {st.session_state.doi}..."):
        schema_version, xml = hairball(
            doi=st.session_state.doi,
            depositor_name=st.session_state.name,
            email_address=st.session_state.email
        )

    st.header("Results")
    if schema_version:
        st.balloons()
        st.success(
            f"Successfully recreated a valid deposit file with schema version {schema_version}"
        )
        st.markdown("Here is a [photo of a random cat](https://genrandom.com/cats/)")  
    else:
        st.warning("We were unable to recreate a valid deposit file using any version of the schema.")
        st.markdown("The file generated below is in the latest version of the Crossref schema, but it is **not valid**. You will need to fix any validation errors before you are able to redeposit it.")
  
    st.markdown("You can download the XML by clicking on the `Download generated XML` button below. Alternatively, you can copy the XML to your clipboard by clicking on the clipboard icon in the top-right of the XML display")
    st.download_button(label="Download generated XML",data=xml,file_name=f"{st.session_state.doi}.xml",)



    md = f"""
```xml

    {xml}

```
"""
    st.markdown(md)
    return


def init_sidebar():
    st.sidebar.image("https://assets.crossref.org/logo/crossref-logo-landscape-200.png")

    st.sidebar.header(APP_NAME)
    st.sidebar.text_input(label="doi", on_change=check_inputs, key="doi")
    st.sidebar.text_input(
        label="email",
        on_change=check_inputs,
        key="email",
        placeholder="labs@crossref.org",
    )
    st.sidebar.text_input(
        label="name", on_change=check_inputs, key="name", placeholder="labs"
    )
    st.sidebar.button(label="Regurgitate",on_click=attempt_to_regurgitate)
    


init_sidebar()


if "first_run" not in st.session_state:
    logger.info("starting new session")
    st.session_state.first_run = True
    st.markdown(load_file("README.md"))
else:
    logger.info("continuing session")
