#

## In the output schema a citation doi gets a "provider" attribute added to it which does not exist in the deposit schema.


```xml
<doi provider="crossref">10.1007/BF01425742</doi>
<unstructured_citation>TEISSIER (B.).-Variétés polaires. Inventiones math. 40, 1977.</unstructured_citation>
</citation>
```

## In output of books the collection element gets a "setbyID" attribute that does not exists in the deposit schema.

```
<collection property="text-mining" setbyID="springer">
```
## In output of books the "<edition_number>:" element is replaced by a "volume" element


## In output of books, publication elements are listed year,month,day andin deposit schema they must be in order day,month,year. See 10.1515/9780691215099

## In output of books the rel:intra_work_relation element gets a 'provider' attribute that is not in the deposit schema. See 10.1515/9780691215099

```
<rel:intra_work_relation identifier-type="doi" provider="Crossref" relationship-type="isIdenticalTo">10.2307/j.ctv10vm1pt.15</rel:intra_work_relation>
```

## In 4.3.4 r we changed the element "name" into "depositor_name" in the header. 



## INteresting DOI examples:

- 10.1023/A:1022653828617   Ancient deposit schema. One of the first ever deposits?