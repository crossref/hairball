# We want to iterate over these from most recent version down to oldest version.

standard = sorted([
    "4.3.0",
    "4.3.1",
    "4.3.2",
    "4.3.3",
    "4.3.4",
    "4.3.5",
    "4.3.6",
    "4.3.7",
    "4.3.8",
    "4.4.0",
    "4.4.1",
    "4.4.2",
    "4.5.0",
    "4.6.0",
    "4.7.0",
    "4.8.0",
    "4.8.1",
    "5.1.0",
    "5.2.0",
    "5.3.0",
    "5.3.1",
],reverse=True)

grants = sorted(["0.0.1", "0.0.2", "0.1.0", "0.1.1"],reverse=True)

crossref_namespaces = ['ai','jats', 'fr', 'mml', 'ct', 'rel']
crossref_namespaces = ['ai','jats', 'fr', 'ct', 'rel']

