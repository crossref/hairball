![Image](https://crossref.org/img/labs/creature2.svg)

# Hairball

> Regurgitate Crossref Deposit XML


### Warnings, Caveats and Weasel Words

This is a [Crossref Labs](https://www.crossref.org/labs/) thang. Back away slowly and don't make eye contact.

## About the name


> **Hairball** : (slang, figuratively) A messy, tangled, intractable, stay-away-from-it issue.

> **Hairball**: A rounded mass of partially digested hair forming in the stomach or intestines of cats and certain other animals that lick their coat.

## Backgound

Is it possible to return XML from our APIs in a format that is easy to redeposit?

No.

Well, not without some work.

Ultimately we need to better harmonize our output schema with
our input schema.

But this example is designed to show that it *is* possible. Perhaps not for 100% of our content- but for a large number of cases.

## Why isn't this easy?

Because we made it hard. Not deliberately. We just did.

Ideally, our deposit process would be [declarative](https://en.wikipedia.org/wiki/Declarative_programming), but it isn't.

We have three broad ways of updating Crossref metadata:

- Members making direct metadata deposits
- Members indirectly manipulate specific elements within already deposited metadata. Critically, Crossref does **not** update the previously deposited XML then they do this.
- Crossref adding metadata elements in addition to the member's metadata (e.g. matched references). Critically, Crossref does **not** update the previously deposited XML then they do this.

So, basically, we don't always have a member-provided XML deposit file that reflects the current state of the metadata within our system. Let's illustrate with an example.

1. Member deposits XML for one DOI. It contains references.
2. As part of the processing, Crossref matches some of those references and adds DOIs to them.
3. A few months later, the member moves the content on their site and sends Crossref a resource update to point to the new location of the content.

Now- this means that, by step 2,  the metadata deposit file that was submitted to Crossref in step 1 no longer reflects the metadata provided to us by the member in step 1.

This means that, in order to provide an XML file after step 3, we have to try to recreate it. And that is what we do. But we don't do it using the deposit schema. Instead, we do it with one of two output schemas (`unixref` & `unixsd`) that are [almost, but not quite, entirely unlike](https://en.wikipedia.org/wiki/Phrases_from_The_Hitchhiker%27s_Guide_to_the_Galaxy#:~:text=In%20chapter%2017%20of%20the,quite%2C%20entirely%20unlike%20tea%22.) the deposit schema.



## What does this set of scripts do?

It tries to bridge the gap between the output schemas and the deposit schema. So it attempts to "fix" a few problems. Note that these problems really *should* be fixed in the CS system itself. Doing it this far downstream is really not ideal. However, I figured that even the process of writing this would help by:

a. Showing it was possible (not quite as big a hairball as we expected)
b. Documenting some of the changes we would need to make in order to support this more robustly.

"hairball" is a streamlit frontend to a script called regurgitator.py. Regurgitator is a command-line script that iterates over a list of DOIs and tries to regenerate a deposit file for each DOI in the lest.

Regurgitator does the following things.

1. It normalized namespaces in the XML. Some of our members seem to insert their own namespaces in their deposits. Apparently, we must strip these and ignore them while processing in the CS system- but then we blindly include them again in the output- which means that the break validation in the deposit schema.
2. We move a bunch of elements that must occur in a particular order in the deposit schema, but which are not returned in the same order in the output. One of the more baffling elements where this happens is `publication_date` when the deposit schema seems to require the subelements to be in the order `month`, `day`, `year` (bloody Americans)- but the output schema outputs them as `day`, `month`, `year`. There seems to be no reason to order the subelements at all (they're labeled in the subelements!)- but there you go.
3. We remove attributes that get added to the output but that are not supported in the deposit schema. The biggest examples are `provider` and `setbyID`. These seem to do essentially the same thing (allow us to indicate when a metadata element has been added by Crossref) but have different names. Arguments could be made both ways with these. One could argue that they should be supported in the deposit schema (though how would we handle versioning for that?). On the other hand, stripping them also seems reasonable as, presumably, if the member redeposits them they then turn into assertions made by the member.

## When **won't** these scripts work?

- When the metadata was deposited a long time ago and we no longer have access to the deposit schema. See [10.1023/A:1022653828617](https://api.crossref.org/works/10.1023/A:1022653828617.xml) as an example.
- When several, mutually dependent elements in the output are not in the expected order that they should appear in when deposited. 
- When you encounter something weird that I haven't encountered yet (there are likely to be many of these).





